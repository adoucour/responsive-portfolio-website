# portfolio-backend Sanity

Congratulations, you have now installed the Sanity Content Studio, an open source real-time content editing environment connected to the Sanity backend.

Now you can do the following things:

- [Read “getting started” in the docs](https://www.sanity.io/docs/introduction/getting-started?utm_source=readme)
- [Join the community Slack](https://slack.sanity.io/?utm_source=readme)
- [Extend and build plugins](https://www.sanity.io/docs/content-studio/extending?utm_source=readme)

### Clone this repository

git clone https://gitlab.com/adoucour/responsive-portfolio-website.git

### Install @sanity/cli

Run `npm install -g @sanity/cli` or `yarn global add @sanity/cli` to install Sanity Cli

### Install dependencies

Run `npm install` to generate node_modules folder who contains dependencies.

## Developement server

Run `npm run start` or `sanity start` for a development server.

## Build

Run `npm run build` or `sanity build` for build this project, build files will be at the root of the project in the dist directory.
