export default {
  name: "abouts",
  title: "Abouts",
  type: "document",
  fields: [
    {
      name: "id",
      title: "Id",
      type: "number",
    },
    {
      name: "imgUrl",
      title: "ImgUrl",
      type: "image",
      options: {
        hotspot: true,
      },
    },
  ],
};
