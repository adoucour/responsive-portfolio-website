# portfolio-backend Sanity

Congratulations, you have now installed the Sanity Content Studio, an open source real-time content editing environment connected to the Sanity backend.

Now you can do the following things:

- [Read “getting started” in the docs](https://www.sanity.io/docs/introduction/getting-started?utm_source=readme)
- [Join the community Slack](https://slack.sanity.io/?utm_source=readme)
- [Extend and build plugins](https://www.sanity.io/docs/content-studio/extending?utm_source=readme)

### Clone this repository

git clone https://gitlab.com/adoucour/responsive-portfolio-website.git

### Install @sanity/cli

Run `npm install -g @sanity/cli` or `yarn global add @sanity/cli` to install Sanity Cli

### Install dependencies

Run `npm install` to generate node_modules folder who contains dependencies.

## Developement server

Run `npm run start` or `sanity start` for a development server.

## Build

Run `npm run build` or `sanity build` for build this project, build files will be at the root of the project in the dist directory.

# portfolio-front

This repository contain the portfolio-front app

## Requirements

To run this application on your workstation, you must install :

-   node.js version 16.xx.xx
-   npm 8.xx.x
-   Vue Cli 5.x.xx
-   Vue 3

### Vue CLI

Run `npm install -g @vue/cli` or `yarn global add @vue/cli`

### Clone this repository

git clone https://gitlab.com/adoucour/responsive-portfolio-website.git

### Install dependencies

Run `npm install` to generate node_modules folder who contains dependencies.

## Developement server

Run `npm run serve` for a development server.

Navigate to `http://localhost:8080/`. The application will automatically reload if you change any of the source files.

## Running unit tests and open coverage files [jest](https://jestjs.io/docs/cli)

Run `npm run jest` to execute  all tests (default).

Run `npm run jest:watch` to watch files for changes and rerun all tests when something changes and display individual test results with the test suite hierarchy.

Run `npm run jest:cov` to execute all tests, to access the coverage details in coverage/Icov-report folder, open in your browser index.html file.

## Running eslint [eslint](https://eslint.org/)

Run `npm run lint` for check js rules.

Run `nom run lint:fix` for fix automatically (note: this command not systematically repair all errors).

## Running prettier [prettier](https://prettier.io/)

Run `npm run prettier:check` for check your format code.

Run ` npm run prettier` for format the code.

## Build

Run `npm run build` for build this project, build files will be at the root of the project in the dist directory.

## Pipeline stages (TO BE DEFINE)

## GitLab CI/CD guides (TO BE DEFINE)

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

-   Now you can run this application by clicking on the following link : http://localhost:8080/

