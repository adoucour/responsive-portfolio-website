module.exports = {
	env: {
		node: true,
		jasmine: true,
	},
	extends: [
		'eslint:recommended',
		'plugin:vue/vue3-essential',
		'plugin:vue/vue3-recommended',
		'plugin:vue/vue3-strongly-recommended',
		'prettier',
	],
	rules: {
		'vue/multi-word-component-names': [
			'error',
			{
				ignores: [],
			},
		],
		'vue/no-unused-vars': 'error',
		'vue/require-default-prop': 'off',
		'vue/no-use-v-if-with-v-for': 'error',
		'vue/no-watch-after-await': 'error',
		'vue/prefer-import-from-vue': 'error',
		'vue/require-prop-type-constructor': 'error',
		'vue/require-valid-default-prop': 'error',
		'vue/use-v-on-exact': ['error'],
		'vue/component-definition-name-casing': ['error', 'kebab-case'],
		'vue/attribute-hyphenation': [
			'error',
			'always',
			{
				ignore: [],
			},
		],
		'vue/prop-name-casing': ['error', 'camelCase'],
		'vue/v-on-style': 'error',
	},
};
