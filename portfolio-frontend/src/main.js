import { createApp } from 'vue';
import App from './App.vue';
import router from './router/index';
import './style.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faGithub,
	faGitlab,
	faLinkedin,
} from '@fortawesome/free-brands-svg-icons';
//import { far } from '@fortawesome/free-regular-svg-icons';
import {
	faBars,
	faX,
	faEye,
	faChevronLeft,
	faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { MotionPlugin } from '@vueuse/motion';

import { createI18n } from 'vue-i18n';
import { languages } from './lang/index';

const messages = Object.assign(languages);

library.add(
	faBars,
	faX,
	faGithub,
	faGitlab,
	faLinkedin,
	faEye,
	faChevronLeft,
	faChevronRight
);

const i18n = createI18n({
	locale: getLocale(),
	messages,
});

const app = createApp(App);
app.use(router);
app.use(MotionPlugin);
app.use(i18n);
app.component('font-awesome-icon', FontAwesomeIcon);
app.mount('#app');

function getLocale() {
	if (localStorage.getItem('lang')) {
		return localStorage.getItem('lang');
	}
	return 'en';
}
