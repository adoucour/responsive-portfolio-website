import { createRouter, createWebHashHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';

const routes = [
	{
		path: '/',
		name: 'home',
		component: HomeView,
	},
	{
		path: '/home',
		component: HomeView,
	},
	{
		path: '/about',
		component: HomeView,
	},
	{
		path: '/work',
		component: HomeView,
	},
	{
		path: '/skills',
		component: HomeView,
	},
	{
		path: '/testimonial',
		component: HomeView,
	},
	{
		path: '/contact',
		component: HomeView,
	},
];

const router = createRouter({
	history: createWebHashHistory(),
	routes,
});

export default router;
