import { createClient } from '@sanity/client';

export const client = createClient({
	projectId: process.env.VUE_APP_SANITY_PROJECT_ID,
	dataset: 'production',
	apiVersion: '2023-03-04',
	useCdn: true,
	token: process.env.VUE_APP_SANITY_TOKEN,
	ignoreBrowserTokenWarning: true,
});
