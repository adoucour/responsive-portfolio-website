import { client } from '../client';

export default {
	addImgUrl(param, data) {
		param.forEach((element) => {
			const img = data.find((img) => img.id === element.id);
			if (img) {
				element.imgUrl = img.imgUrl;
			}
		});
	},
	getImageUrl(imgObj) {
		if (!imgObj) return '';

		if (imgObj?._type === 'image') {
			const imageRef = imgObj.asset._ref
				.replace(/^image-/, '')
				.replace(/-png$/, '.png');
			const sanityCdnUrl = `https://cdn.sanity.io/images/${
				process.env.VUE_APP_SANITY_PROJECT_ID
			}/${client.config().dataset}/`;

			return sanityCdnUrl + imageRef;
		}
		return '';
	},
};
