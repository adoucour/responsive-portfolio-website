import logo from '../assets/logo.png';
import logoCopy from '../assets/logo - Copy.png';
import bgWhite from '../assets/bgWhite.png';
import profile from '../assets/profile.png';
import sass from '../assets/sass.png';
import circle from '../assets/circle.svg';
import bgIMG from '../assets/bgIMG.png';
import angular from '../assets/angular.png';
import vue from '../assets/vue.png';
import email from '../assets/email.png';
import mobile from '../assets/mobile.png';
export default {
	logo,
	logoCopy,
	bgWhite,
	profile,
	sass,
	circle,
	bgIMG,
	angular,
	vue,
	email,
	mobile,
};
