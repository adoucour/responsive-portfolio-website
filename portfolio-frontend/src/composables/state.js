import { readonly, ref } from 'vue';

export function useState() {
	const toogle = ref(false);
	const setToogle = (val) => {
		toogle.value = val;
	};

	return [readonly(toogle), setToogle];
}
