import { shallowMount } from '@vue/test-utils';
import Navbar from '@/components/Navbar/NavbarComponent.vue';
import { MotionPlugin } from '@vueuse/motion';
import { useState } from '@/composables/state';

import { createI18n } from 'vue-i18n';

describe('NavbarComponent.vue Test', () => {
	let wrapper;
	const $i18n = createI18n({
		locale: 'en',
		globalInjection: true,
		messages: {
			en: {
				navbar: ['home', 'about', 'work', 'skills', 'contact'],
			},
			fr: {
				navbar: [
					'accueil',
					'à propos',
					'expériences',
					'compétences',
					'contact',
				],
			},
		},
	});
	const localStorageMock = (() => {
		let store = {};

		return {
			getItem(key) {
				return store[key];
			},

			setItem(key, value) {
				store[key] = value;
			},

			clear() {
				store = {};
			},

			removeItem(key) {
				delete store[key];
			},

			getAll() {
				return store;
			},
		};
	})();

	Object.defineProperty(window, 'localStorage', { value: localStorageMock });

	const setLocalStorage = (id, data) => {
		window.localStorage.setItem(id, data);
	};

	beforeEach(() => {
		window.localStorage.clear();

		wrapper = shallowMount(Navbar, {
			global: {
				plugins: [MotionPlugin, $i18n],
				stubs: ['font-awesome-icon'],
			},
			data() {
				const [toogle, setToogle] = useState(false);
				return {
					navItem: { type: Array, require: true },
					toogle,
					setToogle,
				};
			},
		});
	});
	it('does a wrapper exist', async () => {
		expect(wrapper.exists()).toBe(true);
	});

	it('has data', () => {
		expect(typeof Navbar.data).toBe('function');
		expect(typeof Navbar.beforeMount).toBe('function');
		expect(typeof Navbar.methods).toBe('object');
	});

	it('test navLinks item is not empty with english languages set in localstorage', () => {
		const mockId = 'lang';
		const mockJson = 'en';
		setLocalStorage(mockId, mockJson);
		window.localStorage.setItem(mockId, mockJson);
		expect(localStorage.getItem(mockId)).toEqual(mockJson);

		if (localStorage.getItem(mockId) === mockJson) {
			wrapper.vm.$i18n.locale = mockJson;
		}

		wrapper.vm.setNavItem(mockJson);

		expect(wrapper.vm.navItem).toBeDefined();

		expect(wrapper.vm.navItem).toContain('home');
		expect(wrapper.vm.navItem).toContain('about');
		expect(wrapper.vm.navItem).toContain('work');
		expect(wrapper.vm.navItem).toContain('skills');
		expect(wrapper.vm.navItem).toContain('contact');
		expect(wrapper.vm.navItem).not.toContain('test');
		expect(wrapper.vm.navItem).not.toContain('');

		expect(wrapper.vm.navItem).toEqual([
			'home',
			'about',
			'work',
			'skills',
			'contact',
		]);

		expect(wrapper.vm.navItem).not.toEqual([]);
		expect(wrapper.vm.languageDefault).toBeFalsy();
	});

	it('test navLinks item is not empty with french languages set in localstorage', () => {
		const mockIdFrench = 'lang';
		const mockdataFrench = 'fr';
		setLocalStorage(mockIdFrench, mockdataFrench);
		expect(localStorage.getItem(mockIdFrench)).toEqual(mockdataFrench);

		if (localStorage.getItem(mockIdFrench) === mockdataFrench) {
			wrapper.vm.$i18n.locale = mockdataFrench;
		}

		wrapper.vm.setNavItem(mockdataFrench);

		expect(wrapper.vm.navItem).toContain('accueil');
		expect(wrapper.vm.navItem).toContain('à propos');
		expect(wrapper.vm.navItem).toContain('expériences');
		expect(wrapper.vm.navItem).toContain('compétences');
		expect(wrapper.vm.navItem).toContain('contact');
		expect(wrapper.vm.navItem).not.toContain('test');
		expect(wrapper.vm.navItem).not.toContain('');

		expect(wrapper.vm.navItem).toEqual([
			'accueil',
			'à propos',
			'expériences',
			'compétences',
			'contact',
		]);

		expect(wrapper.vm.navItem).not.toEqual([]);
		expect(wrapper.vm.languageDefault).toBeTruthy();
	});

	it('test navLinks item in english by default, with localstorage empty', () => {
		expect(wrapper.vm.navItem).toBeDefined();

		expect(wrapper.vm.navItem).toContain('home');
		expect(wrapper.vm.navItem).toContain('about');
		expect(wrapper.vm.navItem).toContain('work');
		expect(wrapper.vm.navItem).toContain('skills');
		expect(wrapper.vm.navItem).toContain('contact');
		expect(wrapper.vm.navItem).not.toContain('test');
		expect(wrapper.vm.navItem).not.toContain('');

		expect(wrapper.vm.navItem).toEqual([
			'home',
			'about',
			'work',
			'skills',
			'contact',
		]);

		expect(wrapper.vm.navItem).not.toEqual([]);
		expect(wrapper.vm.languageDefault).toBeFalsy();
	});

	it('test changeLanguage function set in french', () => {
		const changeLanguageSpy = jest.spyOn(wrapper.vm, 'changeLanguage'); // eslint-disable-line
		changeLanguageSpy('fr');
		expect(changeLanguageSpy).toHaveBeenCalled();
		expect(wrapper.vm.navItem).toEqual([
			'accueil',
			'à propos',
			'expériences',
			'compétences',
			'contact',
		]);
		expect(wrapper.vm.navItem).not.toEqual([
			'home',
			'about',
			'work',
			'skills',
			'contact',
		]);
		expect(wrapper.vm.languageDefault).toBeTruthy();
	});

	it('test changeLanguage function set in english', () => {
		const changeLanguageSpy = jest.spyOn(wrapper.vm, 'changeLanguage'); // eslint-disable-line
		changeLanguageSpy('en');
		expect(changeLanguageSpy).toHaveBeenCalled();
		expect(wrapper.vm.navItem).not.toEqual([
			'accueil',
			'à propos',
			'expériences',
			'compétences',
			'contact',
		]);
		expect(wrapper.vm.navItem).toEqual([
			'home',
			'about',
			'work',
			'skills',
			'contact',
		]);
		expect(wrapper.vm.languageDefault).toBeFalsy();
	});

	it('test setToogle useState', () => {
		expect(wrapper.vm.toogle).toBeFalsy();
		expect(wrapper.vm.toogle).not.toBeTruthy();

		wrapper.vm.setToogle(true);
		expect(wrapper.vm.toogle).not.toBeFalsy();
		expect(wrapper.vm.toogle).toBeTruthy();

		wrapper.vm.setToogle(false);
		expect(wrapper.vm.toogle).toBeFalsy();
		expect(wrapper.vm.toogle).not.toBeTruthy();
	});
});
