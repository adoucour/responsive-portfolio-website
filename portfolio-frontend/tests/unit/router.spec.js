import { mount, RouterLinkStub } from '@vue/test-utils';
import { MotionPlugin } from '@vueuse/motion';
import App from '../../src/App.vue';
import HomeView from '../../src/views/HomeView';
import router from '../../src/router/index.js';
import { createI18n } from 'vue-i18n';

describe('Routing test', () => {
	const $i18n = createI18n({
		locale: 'en',
		globalInjection: true,
		messages: {
			en: {
				navbar: ['home', 'about', 'work', 'skills', 'contact'],
			},
			fr: {
				navbar: [
					'accueil',
					'à propos',
					'expériences',
					'compétences',
					'contact',
				],
			},
		},
	});
	it('home route test', async () => {
		const routes = router;

		routes.push('/');

		await routes.isReady();

		const wrapper = mount(App, {
			routes,
			global: {
				plugins: [routes, MotionPlugin, $i18n],
				stubs: {
					'router-link': RouterLinkStub,
					'font-awesome-icon': true,
				},
			},
		});

		expect(wrapper.findComponent(HomeView).exists()).toBe(true);
	});
});
